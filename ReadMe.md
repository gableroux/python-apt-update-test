# Python apt update test

[![pipeline status](https://gitlab.com/gableroux/python-apt-update-test/badges/master/pipeline.svg)](https://gitlab.com/gableroux/python-apt-update-test/commits/master)

Shameless debugging for python debian images

## How I grabbed the list of python images

```bash
skopeo --override-os linux inspect docker://docker.io/python \
  | jq '.RepoTags[]' -r | sed -e '/alpine/d' -e '/windows/d' -e '/onbuild/d'
```

## The problem I was facing

```
root@b0ca6eeb3f75:/# apt update
Get:1 http://security.debian.org jessie/updates InRelease [44.9 kB]
Ign http://deb.debian.org jessie InRelease
Get:2 http://deb.debian.org jessie-updates InRelease [7340 B]
Get:3 http://deb.debian.org jessie Release.gpg [2420 B]
Get:4 http://deb.debian.org jessie Release [148 kB]
Get:5 http://security.debian.org jessie/updates/main amd64 Packages [824 kB]
Get:6 http://deb.debian.org jessie/main amd64 Packages [9098 kB]
Fetched 10.1 MB in 6s (1460 kB/s)
W: Failed to fetch http://deb.debian.org/debian/dists/jessie-updates/InRelease  Unable to find expected entry 'main/binary-amd64/Packages' in Release file (Wrong sources.list entry or malformed file)

E: Some index files failed to download. They have been ignored, or old ones used instead.
root@b0ca6eeb3f75:/#
```

So I'm running the command in all versions, see [`.gitlab-ci.yml`](`.gitlab-ci.yml`).

## License

[MIT](LICENSE.md) © [Gabriel Le Breton](https://gableroux.com)

